# tedit

A useful notes text editor for Ubuntu Touch. Write down things in simple text format. 

Some additional features are:

- calculating hash values
- encrypting of notes
- creating of QR codes
- importing of websites as simple text

For development progress, please check the [changelog](https://gitlab.com/Danfro/tedit/-/blob/main/README.md/CHANGELOG.md).

This app is available in the OpenStore:

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/tedit.danfro)

## Credits

Originally based on the App [edIt](https://launchpad.net/edit) by Paweł Stroka (not released to OpenStore) and brought to OpenStore as [tedit](https://github.com/fulvio999/tedit) by fulvio.
This is the second fork of fulvio's app version to keep the app maintained and available for users.

## File storage location

- In case you wish to sync your files with another app, tedit files are stored in the apps `~/.local/share` folder. This should be `/home/phablet/.local/share/tedit.danfro`. Tedit can't use just "any" folder on your device due to apparmor permission restrictions. Apps generally can only access "their own" folders for cache, config and data. Access to all other folders would need elevated permissions. To be able to browse all folders on a device, the app would need to be unconfined, which is not desired.

- To migrate your notes from the older app version, copy/move them from `/home/phablet/.local/share/tedit.danfro` to `/home/phablet/.local/share/tedit.fulvio` and restart the app. They should now be available.

- If you wish to have the app storage folder in another place, you could make use of `ln` symlinks. First create a symlink of tedits default storage space to another folder e.g. like this:

    ln -s /home/phablet/.local/share/tedit.danfro/ /home/phablet/Documents

  You could then rename the folder `/home/phablet/Documents/tedit.danfro` to another name for instance `/home/phablet/Documents/tedit files`.

- Files can be imported into tedit via content hub. Note, that this will create a copy of the file in tedit's working directory. The original file will remain unmodified. Contenthub export is on the roadmap to be implemented in a future version.

## Translating

Tedit app can be translated on [Hosted Weblate](https://hosted.weblate.org/projects/ubports/tedit/). The localization platform of this project is sponsored by Hosted Weblate via their free hosting plan for Libre and Open Source Projects.

We welcome translators from all different languages. Thank you for your contribution!
You can easily contribute to the localization of this project (i.e. the translation into your language) by visiting (and signing up with) the Hosted Weblate service as linked above and start translating by using the webinterface. To add a new language, log into weblate, goto tools --> start new translation.