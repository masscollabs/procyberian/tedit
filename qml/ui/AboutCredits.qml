/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

Item {
    id: aboutCreditsPage

    Flickable {
        id: page_flickable

        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors.fill: parent
        contentHeight:  contentColumn.height + units.gu(2)
        clip: true

        Column {
            id: contentColumn
            // spacing: units.gu(2)
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
            }
            width: parent.width

            Label {
                id: actualdev

                text: "\n" + i18n.tr("App development since version v3.0.0")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }

            ListItem {
                divider { visible: false; }
                height: l_maintainer.height + divider.height
                ListItemLayout {
                    id: l_maintainer
                    title.text: i18n.tr("Maintainer") + ": Daniel Frost"
                    ProgressionSlot {name: "external-link"; }
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Danfro')}
            }

            ListItem {
                divider { visible: false; }
                height: gitlabGraphs.height + divider.height
                ListItemLayout {
                    id: gitlabGraphs
                    title.text: i18n.tr("Contributors: are listed on Gitlab")
                    ProgressionSlot {name: "external-link"; }
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Danfro/tedit/-/graphs/main')}
            }

            ListItem {
                divider { visible: false; }
                height: weblateContributors.height + divider.height
                ListItemLayout {
                    id: weblateContributors
                    title.text: i18n.tr("Translators: are listed on weblate")
                    subtitle.text: i18n.tr("(weblate login required)")
                    ProgressionSlot {name: "external-link"; }
                }
                onClicked: {Qt.openUrlExternally('https://hosted.weblate.org/user/?q=%20contributes:ubports/tedit')}
            }

            ListItem {
                divider { visible: false; }
                height: testingCreadits.height + divider.height
                ListItemLayout {
                    id: testingCreadits
                    title.text: i18n.tr("Special thanks:")
                    // TRANSLATORS: %1 will be replaced with the (user)name
                    summary.text: i18n.tr("%1 for testing and ideas").arg("@domubpkm")
                }
            }

            Rectangle {
                // spacer
                width: parent.width
                height: units.gu(2)
                color: "transparent"
            }

            Label {
                id: historicaldev

                text: "\n" + i18n.tr("App development up to version v2.8.4")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }

            Repeater {
                id: historicalDevCredits

                model: [
                    { name: i18n.tr("Author") + ": fulvio", url: "https://github.com/fulvio999" },
                    { name: i18n.tr("Author") + ": Paweł Stroka" }, 
                    { name: i18n.tr("Code") + "  : " + i18n.tr("jshashes library for hash Algorithm"), url: "https://github.com/h2non/jshashes" },
                    { name: i18n.tr("Code") + "  : " + i18n.tr("Base64 features"), url: "https://github.com/mathiasbynens/base64" },
                    { name: i18n.tr("Code") + "  : " + i18n.tr("M4rtinK, QML binding for qr.js lib"), url: "https://github.com/M4rtinK" },
                    { name: i18n.tr("Code") + "  : " + i18n.tr("Developers of CriptoJS library"), url: "https://cryptojs.gitbook.io/docs/" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        ProgressionSlot {name: "external-link"; visible: modelData.url ? true : false }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            ListItem {
                divider { visible: false; }
                height: githubGraphs.height + divider.height
                ListItemLayout {
                    id: githubGraphs
                    title.text: i18n.tr("Contributors: are listed on Github")
                    ProgressionSlot {name: "external-link"; }
                }
                onClicked: {Qt.openUrlExternally('https://github.com/fulvio999/tedit/graphs/contributors')}
            }

        }
    }
}
