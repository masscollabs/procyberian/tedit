/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

Item {
    id: aboutImportantNotesPage

    Flickable {
        id: page_flickable

        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors.fill: parent
        contentHeight:  contentColumn.height + units.gu(2)
        clip: true

        Column {
            id: contentColumn
            spacing: units.gu(2)
            anchors {
                left: parent.left
                right: parent.right
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
                top: parent.top
                topMargin: units.gu(2)
            }
            width: parent.width
            Label {
                id: noteHeader

                text: i18n.tr("IMPORTANT note on encrypted files")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }
            Label {
                id: noRestoreNote

                font.bold : true
                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: i18n.tr("If the encryption key is lost, there is NO restore. The file remains encrypted! The app author takes no responsibility!")
                wrapMode: Text.WordWrap
            }
            Label {
                id: encrytionNote

                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: i18n.tr("Encrypted file names have a \"XXX\" suffix. A padlock is shown, when an encrypted file is currently open.")
                wrapMode: Text.WordWrap
            }
            Label {
                id: storageHeader

                visible: header_sections.selectedIndex === 1
                text: i18n.tr("File storage information")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }
            Label {
                id: storageLocationNote

                visible: header_sections.selectedIndex === 1
                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: i18n.tr("Files are stored in the following folder on your device:") 
                wrapMode: Text.WordWrap
            }
            Label {
                id: storageNote

                visible: header_sections.selectedIndex === 1
                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: root.fileSavingPath
                font.italic: true
                color: theme.palette.normal.activity 
                wrapMode: Text.WordWrap
            }
            Label {
                id: storageReasonNote

                visible: header_sections.selectedIndex === 1
                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: i18n.tr("Tedit can't use just \"any\" folder on your device due to apparmor permission restrictions. By default on Ubuntu Touch apps can only access \"their own\" folders for cache, config and data. Access to all other folders would need elevated permissions.")
                wrapMode: Text.WordWrap
            }
            Label {
                id: contentHubNote

                visible: header_sections.selectedIndex === 1
                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: i18n.tr("Files can be imported into tedit via content hub. Note, that this will create a copy of the file in tedit's working directory. The original file will remain unmodified. Contenthub export is on the roadmap to be implemented in a future version.")
                wrapMode: Text.WordWrap
            }
            Label {
                id: symlinkNote

                visible: header_sections.selectedIndex === 1
                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: i18n.tr("If you wish to have the app storage folder in another place, you could make use of `ln` symlinks. Create a symlink of tedits default storage space to another folder e.g. with the command below. You can then rename that linked folder to your convenience.")
                wrapMode: Text.WordWrap
            }
            Label {
                id: symlinkCommand

                visible: header_sections.selectedIndex === 1
                horizontalAlignment: Text.AlignJustify
                width: parent.width
                text: i18n.tr("ln -s /home/phablet/.local/share/tedit.danfro/ /home/phablet/Documents")
                font.italic: true
                color: theme.palette.normal.activity 
                wrapMode: Text.WordWrap
            }
        }
    }
}
