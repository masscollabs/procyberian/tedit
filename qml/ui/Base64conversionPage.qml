/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItem
import Lomiri.Components.Popups 1.3
import Lomiri.Content 1.1

import "../components"
import "../js/base64.js" as Base64enc

/*
Page that display the result of Base 64 encoding/decoding of the text are content
*/
Page {
    id: base64ConversionPage
    visible: false

    header: PageHeader {
    id: header
    title : i18n.tr("Base 64 encoder/decoder")

    trailingActionBar.actions: [
            Action {
                id: clearAllAction
                text: i18n.tr("Clear all")
                iconName: "delete"
                onTriggered: clearText()
            },
            Action {
                id: selectAllAction
                text: i18n.tr("Select all")
                iconName: "edit-copy"
                onTriggered: {
                    resultTextArea.selectAll();
                    resultTextArea.copy();
                    showInfo(i18n.tr("Conversion result copied to clipboard"));
                }
            }
        ]
    }

    function clearText() {
        resultTextArea.text = ""
        inputTextArea.text = ""
    }

    Component.onCompleted: clearText()

    /* the index of currenlty select item in the conversion combo box */
    property int currentConversionItemSelected : 1;

    Component {
        id: confirmSavingNoteBase64
        ConfirmSavingNoteBase64 { }
    }

    Rectangle {
        id: backgroundFilling
        anchors.fill: parent
        color: settings.pageBackgroundColor
    }

    Column {
        anchors.fill: parent
        spacing: units.gu(1)

        anchors {
            margins: units.gu(1)
        }

        Component {
            id: conversionSelectorDelegate
            OptionSelectorDelegate { text: name; subText: description; }
        }

        /* The available conversions shown in the combo box */
        ListModel {
            id: conversionTypeModel
        }

        /* fill listmodel using this method because allow you to use i18n */
        Component.onCompleted: {
            conversionTypeModel.append( { name: "<b>"+i18n.tr("Encode")+"</b>", description: i18n.tr("encode to Base 64"), value:1 } );
            conversionTypeModel.append( { name: "<b>"+i18n.tr("Decode")+"</b>", description: i18n.tr("decode from Base 64"), value:2 } );
        }

        Rectangle {
            id: placeHolderRectangle
            color: settings.pageBackgroundColor
            width: parent.width
            height: units.gu(6)
        }

        Rectangle {
            id: titleContainer
            color: settings.pageBackgroundColor
            width: parent.width
            height: conversionTypeItemSelector.height

            /* encode/decode conversion selector */
            ListItem.ItemSelector {
                id: conversionTypeItemSelector
                anchors.topMargin: units.gu(1)
                anchors.leftMargin: units.gu(1)
                anchors.rightMargin: units.gu(1)
                delegate: conversionSelectorDelegate
                model: conversionTypeModel
                containerHeight: itemHeight * 2

                /* ItemSelectionChange event is not built-in with ItemSelector component: use a workaround */
                onDelegateClicked: {
                    resultTextArea.text = ""  //clean
                    if(conversionTypeItemSelector.currentlyExpanded.toString() != 'false'){
                        /* console.log("Selected index: "+selectedIndex); */
                        if(currentConversionItemSelected !== selectedIndex){
                            currentConversionItemSelected = selectedIndex;
                        }
                    }
                }
            }
        }

        Button {
            id: importTextButton
            anchors.horizontalCenter: parent.horizontalCenter
            text: i18n.tr("Import content of current note")
            onClicked: inputTextArea.text = textArea.text
        }

        Rectangle {
            id: inputToConvertContainer
            color: settings.pageBackgroundColor
            width: parent.width
            height: parent.height/2 - placeHolderRectangle.height - units.gu(10)

            /* Display the file content */
            TextArea {
                id: inputTextArea
                width: parent.width
                height: parent.height
                textFormat: TextEdit.AutoText
                font.pixelSize: settings.pixelSize
                placeholderText: i18n.tr("Insert here the content to encode/decode")
                inputMethodHints: Qt.ImhNoPredictiveText
                selectByMouse: true
                wrapMode: settings.wordWrap ? TextEdit.Wrap : TextEdit.NoWrap
            }
        }

        Rectangle {
            id: conversionResultContainer
            color: settings.pageBackgroundColor
            width: parent.width
            height: inputToConvertContainer.height

            /* Display the conversion result */
            TextArea {
                id: resultTextArea
                width: parent.width
                height: parent.height
                text: "" // always start with empty text field
                textFormat: TextEdit.AutoText
                font.pixelSize: settings.pixelSize
                placeholderText: i18n.tr("Conversion Result")
                inputMethodHints: Qt.ImhNoPredictiveText
                selectByMouse: true
                wrapMode: settings.wordWrap ? TextEdit.Wrap : TextEdit.NoWrap
                onTextChanged: resultTextArea.length > 0 ? doConversionContainer2.visible = true : doConversionContainer2.visible = false
            }
        }

        Row {
            id: doConversionContainer
            height: units.gu(3)
            spacing: units.gu(1)
            anchors.horizontalCenter: parent.horizontalCenter

            Button {
                id: doConversionButton
                anchors.verticalCenter: parent.bottom
                text: i18n.tr("Convert")
                width: units.gu(20)
                color: theme.palette.normal.positive
                onClicked: {

                    if(inputTextArea.text.length > 0)
                    {
                        resultTextArea.text = ""  //clean
                        try {
                            if (conversionTypeModel.get(conversionTypeItemSelector.selectedIndex).value === 1){
                                // Encode to Base64
                                resultTextArea.text = Base64enc.encode(inputTextArea.text)
                            }else if(conversionTypeModel.get(conversionTypeItemSelector.selectedIndex).value === 2){
                                // Decode from Base64
                                resultTextArea.text = Base64enc.decode(inputTextArea.text)
                            }

                        } catch (InvalidCharacterError) { /* Exception thrown from base64.js file */
                            resultTextArea.text = i18n.tr("Invalid character: the string to be decoded is not correctly encoded")
                        }
                    } else {
                        resultTextArea.text = i18n.tr("Input is empty!");
                    }
                }
            }
        }

        Row {
            id: doConversionContainer2
            visible: false
            height: units.gu(5)
            spacing: units.gu(1)
            anchors.horizontalCenter: parent.horizontalCenter

            /* add the Conversion result at the existing text (if any) in the note area */
            Button {
                id:addResultToNoteButton
                anchors.verticalCenter: parent.bottom
                text: i18n.tr("Add to existing note")
                width: units.gu(20)
                onClicked: {
                    textArea.text = textArea.text + "\n" + resultTextArea.text
                    clearText()
                    pageStack.pop(base64ConversionPage)
                }
            }

            /* add the Conversion result to a NEW note */
            Button {
                id:addResultToNewNoteButton
                anchors.verticalCenter: parent.bottom
                text: i18n.tr("Add to new note")
                width: units.gu(20)
                onClicked: {

                    if (mainPage.saved == false)
                    {
                        pageStack.pop(base64ConversionPage)
                        PopupUtils.open(confirmSavingNoteBase64);

                    } else {
                        /* note already saved: just create a new one */
                        mainPage.saved = false
                        mainPage.openedFileName = ""
                        mainPage.currentFileLabelVisible = false
                        textArea.text = resultTextArea.text
                        clearText()
                        pageStack.pop(base64ConversionPage)
                    }
                }
            }
        }
    }
}
