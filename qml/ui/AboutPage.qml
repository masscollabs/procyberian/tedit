/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import Lomiri.Components 1.3


Page {
    id: aboutPage
    anchors.fill: parent
    visible: false

    header: PageHeader {
        id: header
        title: i18n.tr('About')
    }

    Sections {
        id: header_sections
        width: parent.width  // needed, otherwise the sections are not horizontally swipeable
        anchors {
            top: header.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }
        model: [i18n.tr("General"), i18n.tr("Important notes"), i18n.tr("Credits")]
    }

    StackLayout {
        id: aboutLayout
        anchors {
            top: header_sections.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        currentIndex: header_sections.selectedIndex
        AboutGeneral {}
        AboutImportantNotes {}
        AboutCredits {}
    }
}
