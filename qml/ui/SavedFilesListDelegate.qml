/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * The app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.12
import Lomiri.Components 1.3
import tedit 1.0
import Lomiri.Components.ListItems 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Content 1.1

/*
   Delegate object used to display the locally saved files in a ListItem
*/
ListItem {
    id: standardItem
    width: localFilePickerPage.width
    anchors.horizontalCenter: parent.Center

    Image {
        id: padlockIcon
        visible: file.includes("XXX")
        source: Qt.resolvedUrl("../../assets/encrypted.png")
        height: units.gu(2.5)
        width: visible ? height : 0

        anchors {
            left: parent.left
            leftMargin: units.gu(2)
            verticalCenter: parent.verticalCenter
        }
    }

    Label {
        id: fileLabel
        anchors {
            left: padlockIcon.right
            leftMargin: units.gu(1)
            right: openForEditButton.left
            rightMargin: units.gu(1)
        }
        verticalAlignment: Text.AlignVCenter
        text: file
        font.bold: true
        height: parent.height
        width: file.includes("XXX") ? parent.width - units.gu(12) : parent.width - units.gu(5) // edit button and margin and padlock icon when encrypted
        horizontalAlignment: Text.AlignHCenter
        elide: Text.ElideRight
        Component.onCompleted: if (settings.highlightEncrypted && file.includes("XXX")) color = "#972b74" //"#972b53"
        // do not use a theme based conditional color, this takes too long to render the list
    }

    Button {
        id: openForEditButton
        anchors {
            right: parent.right
            rightMargin: units.gu(2)
            verticalCenter: parent.verticalCenter
        }
        width: units.gu(4)
        height: width
        iconName: "edit"
        color: theme.palette.normal.foreground
        onClicked: {
            lomiriListView.currentIndex = index
            localFilePickerPage.selectedFileIndex = index

            var targetFileName = "file://" + root.fileSavingPath + file;
            /* console.log("File to edit:"+targetFileName); */
            if (targetFileName.includes("XXX")) {  /* file is encrypted */
                PopupUtils.open(decryptKeyInput)
            } else {
                textArea.text = fileIO.read(targetFileName);
                mainPage.title = fileIO.getFullName(targetFileName);
                mainPage.saved = true  /* ie: file NOT modified yet */
                mainPage.openedFileName = file;
                mainPage.fileEncrypted = false;
                mainPage.encryptionPassword = "";
                mainPage.currentFileLabelVisible = true
                pageStack.pop();
            }
        }
    }

    MouseArea {
        id: itemSelectMouseArea
        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
        }
        width: parent.width - units.gu(6) // openForEditButton width + left margin
        onClicked: {
            /* move the highlight component to the currently selected item */
            lomiriListView.currentIndex = index
        }
    }

    /* Swipe to right movement: delete file */
    leadingActions: ListItemActions {
        actions: [
           Action {
               iconName: "delete"
               onTriggered: {
                   lomiriListView.currentIndex = index
                   localFilePickerPage.selectedFileIndex = index
                   PopupUtils.open(confirmDeleteFileDialog)
               }
           }
        ]
     }

    /* Swipe to right movement: edit file */
    trailingActions: ListItemActions {
        actions: [
            Action {
                iconName: "info"
                onTriggered: {
                    /* full path of the file */
                    var targetFileName = "file://" + root.fileSavingPath + file;
                    var fileSize = fileIO.getSize(targetFileName); /* bytes */
                    var lastModifiedDate = fileIO.getFileLastModified(targetFileName);

                    localFilePickerPage.selectedFileModificationDate = lastModifiedDate;
                    localFilePickerPage.selectedFileSize = fileSize;
                    PopupUtils.open(fileInfoPopUp);
                }
            }
        ]
    }
}
