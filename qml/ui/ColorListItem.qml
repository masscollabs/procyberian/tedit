/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3 as ListItem


/*
   Item used in the SettingPage to provide color choice at the user
 */
ListItem.Standard {

    property string label
    property color itemColor

    id: listItem
    text: label
    control: Rectangle {
        radius: 25
        color: itemColor
        border.color: "grey"
        border.width : units.gu(0.1)
        width: units.gu(4)
        height: units.gu(4)
    }
}
