
/*
   Execute an Ajax call to read as text the content of the web site with the given url
 */
function importSiteText(url) {

        var request = new XMLHttpRequest();
        var requestStatus
        request.open("get", url);
        request.setRequestHeader("Content-Encoding", "UTF-8");
        request.onreadystatechange = function() {
            // requestStatus = request.status
            if (request.readyState == XMLHttpRequest.DONE) {
                requestStatus = request.status
                // https://www.w3schools.com/xml/ajax_xmlhttprequest_response.asp
                // 200: status = OK
                if (requestStatus === 200) {
                    textArea.text = request.responseText;
                    PopupUtils.close(importDialogue)
                } else {
                    // if status is not OK, then give an error message
                    console.log("Other: " + requestStatus)
                    showInfo(i18n.tr("The download request did return an error. Please make sure the url is correct and the website is available."))
                }
            }
        }
        request.send();
}
