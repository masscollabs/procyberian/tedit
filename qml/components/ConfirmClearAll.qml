/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

/* Ask confirmation before clear Text Area content */
Dialog {
    id: confirmDialogue
    title: i18n.tr("Confirmation")
    text: i18n.tr("Clear all content?")

    Row{
        spacing: units.gu(2)
        Button {
            text:  i18n.tr("Close")
            width: units.gu(14)
            onClicked: PopupUtils.close(confirmDialogue)
        }
        Button {
            text: i18n.tr("Yes")
            color: theme.palette.normal.negative
            width: units.gu(14)
            onClicked: {
                textArea.text = ""
                PopupUtils.close(confirmDialogue)
            }
        }
    }
}
