/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import "../js/SiteImporter.js" as SiteImporter


/*
   Ask for a web site url where import is text and place in the text Area
 */
Dialog {
   id: importDialogue
   title: i18n.tr("Import a website as text")
   contentWidth: root. width - units.gu(5)

   Component.onCompleted: webSiteUrlText.forceActiveFocus()

   Column {
      id: myDoctorPageColumn
      spacing: units.gu(1.5)

      Row {
         anchors.horizontalCenter: parent.horizontalCenter
         TextField {
            id: webSiteUrlText
            width: importDialogue.width - units.gu(8)
            text: ""
            placeholderText: i18n.tr("Enter a valid http(s) url")
            hasClearButton: false
            inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
         }
      }

      Row {
         anchors.horizontalCenter: parent.horizontalCenter
         spacing: units.gu(1)

         Button {
            text: i18n.tr("Cancel")
            width: units.gu(14)
            onClicked: PopupUtils.close(importDialogue)
         }

         Button {
            text: i18n.tr("Import")
            color: theme.palette.normal.positive
            width: units.gu(17)
            onClicked: {
               if (webSiteUrlText.text !=="") {
                  mainPage.saved = false
                  mainPage.openedFileName = ""
                  mainPage.currentFileLabelVisible = false
                  textArea.text = ""
                  SiteImporter.importSiteText(webSiteUrlText.text);
               }
            }
         }
      }
   } //col
}
