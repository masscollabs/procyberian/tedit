/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import tedit 1.0

/*
   Ask for the encryption key to decrypt a file
 */
Dialog {
    id: decryptKeyInputDialogue
    title: i18n.tr("Enter decrypt key")
    contentWidth: root.width - units.gu(5)

    Component.onCompleted: decryptionKey.forceActiveFocus()

    /* the selected file index position in the ListModel */
    property string fileListModelIndex
    property string filePath

    Column {
        id: pageColumn
        spacing: units.gu(1.5)

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: units.gu(1)
            TextField {
                id: decryptionKey
                width: decryptKeyInputDialogue.width - units.gu(12) // 2 units for the icon width, 1 unit for the spacing, 5 units for margins left/right
                placeholderText: i18n.tr("Decryption key")
                hasClearButton: true
                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                echoMode: TextInput.Password
            }
            Icon {
                id: showPassword
                anchors.verticalCenter: parent.verticalCenter
                width: units.gu(2.5)
                height: width
                name: decryptionKey.echoMode == TextInput.Password ? "view-on" : "view-off"
                MouseArea {
                    anchors.fill: parent
                    onClicked: decryptionKey.echoMode == TextInput.Password ? decryptionKey.echoMode = TextInput.Normal : decryptionKey.echoMode = TextInput.Password
                }
            }
        }

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: units.gu(1)

            Button {
                text: i18n.tr("Close")
                width: units.gu(14)
                onClicked: PopupUtils.close(decryptKeyInputDialogue)
            }

            Button {
                text: i18n.tr("Proceed")
                color: theme.palette.normal.positive
                width: units.gu(17)
                onClicked: {
                    var targetFileName
                    var encryptedContent
                    var decryptedText
                    if (filePath) {
                        // this branch for content hub imported files
                        targetFileName = filePath.split("/").pop()
                        encryptedContent = fileIO.read(filePath).toString();
                        fileIO.write("file://" + root.fileSavingPath + targetFileName, encryptedContent)
                    } else if (fileListModelIndex) {
                        // this branch for tedit open file dialog files
                        targetFileName = localFileslistModel.get(fileListModelIndex).file; /* fileName without path */
                        encryptedContent = fileIO.read(root.fileSavingPath + targetFileName).toString();
                    } else {
                        showInfo(i18n.tr("Please specify a file name first"))
                    }
                    decryptedText = decrypt(encryptedContent, decryptionKey.text);
                    if (decryptedText == '') {
                        messageLabel.text = i18n.tr("Wrong Password");
                        messageLabel.color = theme.palette.normal.negative
                    } else {
                        textArea.text = decryptedText;
                        mainPage.title = fileIO.getFullName(targetFileName);
                        mainPage.saved = true  /* ie: file is NOT modified yet */
                        mainPage.openedFileName = targetFileName;
                        mainPage.currentFileLabelVisible = true
                        mainPage.fileEncrypted = "Encrypted";
                        mainPage.encryptionPassword = decryptionKey.text;

                        PopupUtils.close(decryptKeyInputDialogue)
                        if (!filePath) {
                            /* only pageStack.pop when coming from open file dialog
                               when filePath is set, we have an incoming from content hub
                               then don't .pop(), otherwise this will kill MainView aka root
                            */
                            pageStack.pop();
                        } else {
                            /* on successfully opening an encrypted file, close the file picker page */
                            pageStack.pop(localFilePickerPage)
                        }
                    }
                }
            }
        }

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: units.gu(1)
            Label{
                id: messageLabel
                text:" "
            }
        }
    }
}
