/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

/* import folder */
import "../ui"

/*
   Ask for a web site url to generate the corresponding QR Code
*/
Dialog {
    id: importDialogue
    title: i18n.tr("Generate QR Code")
    contentWidth: root.width - units.gu(5)

    Component.onCompleted: qrContent.forceActiveFocus()

    Column {
        id: qrPageColumn
        spacing: units.gu(1.5)

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            TextField {
                id: qrContent
                width: importDialogue.width - units.gu(8)
                text: ""
                placeholderText: i18n.tr("Enter qr code content")
                hasClearButton: false
                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
            }
        }

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: units.gu(1)

            Button {
                text: i18n.tr("Close")
                width: units.gu(14)
                onClicked: PopupUtils.close(importDialogue)
            }

            Button {
                text: i18n.tr("Generate")
                color: qrContent.length > 0 ? theme.palette.normal.positive : theme.palette.disabled.positive
                width: units.gu(17)
                onClicked: {
                    if (qrContent.text !=="") {
                        PopupUtils.close(importDialogue)
                        pageStack.push(Qt.resolvedUrl("../ui/QrCodeGeneratorPage.qml"),
                            {
                                /* <pag-variable-name>:<property-value from db> */
                                qrcodeContent: qrContent.text
                            }

                        );
                    }
                }
            }
        }
    } //col
}
