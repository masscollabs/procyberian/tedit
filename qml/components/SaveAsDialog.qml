/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

/*
  Popup shown when the user choose "Save" or "Save as" menu entry
*/
Dialog {
    id: dialogue
    title: quitOnSaved ? i18n.tr("Exiting app") : i18n.tr("Save as")
    text: quitOnSaved ? i18n.tr("There are unsaved changes. If you wish to save them, please enter a filename.") :i18n.tr("Enter a name for the file to save.")

    property bool quitOnSaved: false

    Component.onCompleted: fileName.forceActiveFocus()

    // ensures "save" can only be pressed when file name and - if applicable - password have been entered
    function checkConditions() {
        if (fileName.length > 0) {
            if (enableEncryptionCheckBox.checked) {
                if (encryptionPasswordText.length >= settings.minPasswordLength) {
                    saveButton.enabled = true
                    saveButton.text = quitOnSaved ? i18n.tr("Save and exit") : i18n.tr("Save")
                    saveButton.color = theme.palette.normal.positive
                } else {
                    saveButton.enabled = false
                    saveButton.text = i18n.tr("Password too short")
                    saveButton.color = theme.palette.disabled.positive
                }
            } else {
                saveButton.enabled = true
                saveButton.text = quitOnSaved ? i18n.tr("Save and exit") : i18n.tr("Save")
                saveButton.color = theme.palette.normal.positive
            }
        } else {
            saveButton.enabled = false
            saveButton.text = i18n.tr("File name required")
            saveButton.color = theme.palette.disabled.positive
        }

    }

    TextField {
        id: fileName
        text: "" //root.openedFileName
        inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
        onTextChanged: checkConditions()
    }

    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: units.gu(0.5)

        CheckBox {
            id: txtExtension
            checked: true
            onCheckedChanged: {
                /* to have mutual exclusion */
                if (checked) {
                    encryptionPasswordText.enabled = true
                } else {
                    encryptionPasswordText.enabled = false
                }
            }
        }
        Label {
            text: i18n.tr("add \'.txt' as extension")
        }
    }

    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: units.gu(0.5)

        CheckBox {
            id: enableEncryptionCheckBox
            checked: false
            onCheckedChanged: {
                /* to have mutual exclusion */
                if (checked) {
                    encryptionPasswordText.enabled = true
                    checkConditions()
                } else {
                    encryptionPasswordText.enabled = false
                }
            }
        }
        Label {
            text: i18n.tr("Save as encrypted file")
        }
    }

    Row {
        spacing: units.gu(1)
        TextField {
            id:encryptionPasswordText
            width: saveButton.width - units.gu(3.5) // 2.5 units for the icon width, 1 unit for the spacing
            enabled: false
            placeholderText: i18n.tr("Encryption password")
            inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
            echoMode: TextInput.Password
            onTextChanged: checkConditions()
        }
        Icon {
            id: showPassword
            anchors.verticalCenter: parent.verticalCenter
            width: units.gu(2.5)
            height: width
            name: encryptionPasswordText.echoMode == TextInput.Password ? "view-on" : "view-off"
            MouseArea {
                anchors.fill: parent
                onClicked: encryptionPasswordText.echoMode == TextInput.Password ? encryptionPasswordText.echoMode = TextInput.Normal : encryptionPasswordText.echoMode = TextInput.Password
            }
        }
    }

    Button {
        id: saveButton
        enabled: false
        text: i18n.tr("File name required")
        color: theme.palette.disabled.positive

        /*
            in disabled state the buttons label is barely readable
            until this is fixed in the toolkit, the following label
            provides the button text in better readable colors
        */
        Label {
            id: disabledLabel
            visible: !saveButton.enabled
            anchors.centerIn: parent
            width: parent.width
            height: parent.height
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: saveButton.text
        }

        onClicked: {

            /* eg: newFileName: file:///tmp/.local/share/tedit.danfro/due
                console.log("fileIO.getHomePath():"+fileIO.getHomePath());
            */

            var newFileName;
            var contentToSave;

            if ( enableEncryptionCheckBox.checked) {  /* save as encrypted */
                contentToSave = encrypt(textArea.text, encryptionPasswordText.text);
                /* append XXX suffix to file name */
                newFileName = "file://" + root.fileSavingPath+fileName.text + "-XXX";
                mainPage.fileEncrypted = true;
                mainPage.encryptionPassword = encryptionPasswordText.text;

            } else {  /* save as plain text */
                //console.log("Saving As NOT encrypted")
                contentToSave = textArea.text;
                newFileName = "file://" + root.fileSavingPath+fileName.text;
                mainPage.encryptionPassword = "";
                mainPage.fileEncrypted = false;
            }

            if (txtExtension) {
                newFileName = newFileName + ".txt"
            }

            if (!fileIO.write(newFileName, contentToSave)) {
                showInfo(i18n.tr("Couldn't write"));
            } else {
                mainPage.saved = true;
                mainPage.openedFileName = fileIO.getFullName(newFileName)
                mainPage.title = mainPage.openedFileName //fileIO.getFullName(newFileName) /* file name without path */
                mainPage.currentFileLabelVisible = true
            }
            PopupUtils.close(dialogue)
            if (quitOnSaved) Qt.quit()
        }
    }

    Button {
        text: quitOnSaved ? i18n.tr("Exit without saving") : i18n.tr("Cancel")
        onClicked: quitOnSaved ? Qt.quit() : PopupUtils.close(dialogue)
    }

    Button {
        text: i18n.tr("Cancel")
        visible: quitOnSaved
        onClicked: PopupUtils.close(dialogue)
    }
 }
