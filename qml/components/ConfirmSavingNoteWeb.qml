/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

/*
   Ask the user if wants to save or not the current note before closing or replacing it
*/
Dialog {
    id: confirmSaveDialogue
    title: i18n.tr("Confirmation")
    text: i18n.tr("There are unsaved changes in the current note. What do you want to do?")

    Column {
        spacing: units.gu(3)

        /* user want save note content: save current note and proceed then*/
        Button {
            text: i18n.tr("Save and proceed")
            color: theme.palette.normal.positive
            width: parent.width
            onClicked: {
                if (mainPage.openedFileName == "") {
                    // Note content was never saved (is new) show saving form
                    PopupUtils.close(confirmSaveDialogue)
                    PopupUtils.open(webSiteSelector)
                    PopupUtils.open(saveAsDialog)
                } else {
                    // Note already created: just save it
                    saveExistingFile(mainPage.openedFileName,root.fileSavingPath)
                    PopupUtils.close(confirmSaveDialogue)
                    PopupUtils.open(webSiteSelector)
                }
            }
        }

        /* user DOESN'T want to save old note: loose unsaved content */
        Button {
            text: i18n.tr("Drop changes and proceed")
            width: parent.width
            onClicked: {
                PopupUtils.close(confirmSaveDialogue)
                PopupUtils.open(webSiteSelector);
            }
        }
    }
}
