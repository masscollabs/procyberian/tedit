/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

/*
   Alert the user about unsaved changes at the currently opened file and ask what want do:
   - save with the current fileName
   - save with anotehr name
   eg: user have a modified file and try to open a new one
*/
Component {
    Dialog {
        id: dialogue
        title: i18n.tr("Unsaved changes")
        text: i18n.tr("There are unsaved changes. Do you want to save this file before closing?")

        property bool quitOnSaved: false

        Component.onCompleted: dialogue.forceActiveFocus()

        Button {
            text: i18n.tr("Save")
            visible: !quitOnSaved
            color: theme.palette.normal.positive
            onClicked: {
                PopupUtils.close(dialogue)

                if(mainPage.openedFileName == "") {                  
                    PopupUtils.open(saveAsDialog)
                } else {
                    /* js function in Main.qml file */
                    saveExistingFile(mainPage.openedFileName,root.fileSavingPath)
                }
            }
        }
        Button {
            text: i18n.tr("Save as")
            visible: !quitOnSaved
            color: theme.palette.normal.positive
            onClicked: {
                PopupUtils.close(dialogue)
                PopupUtils.open(saveAsDialog)
            }
        }
        Button {
            text: i18n.tr("Save and exit")
            visible: quitOnSaved
            color: theme.palette.normal.positive
            onClicked: {
                /* js function in Main.qml file */
                saveExistingFile(mainPage.openedFileName,root.fileSavingPath)
                quitTimer.start()
            }
        }
        Button {
            text: quitOnSaved ? i18n.tr("Exit without saving") : i18n.tr("Close without saving")
            onClicked: {
                quitOnSaved ? Qt.quit() : PopupUtils.close(dialogue)
            }
        }
        Timer {
            // the r/w process when saving a file does take a small time
            // so we allow a small delay after pressing the button before the next action
            id: quitTimer
            interval: 350
            running: false
            repeat: false
            onTriggered: Qt.quit();
        }
    }
}
