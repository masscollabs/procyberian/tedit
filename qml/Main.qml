/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import QtQuick.Controls 2.12 as QQ
import tedit 1.0
import Lomiri.Content 1.1
import Lomiri.Components.Popups 1.3
import Qt.labs.settings 1.0
import Qt.labs.platform 1.0 as PF //for StandardPaths
import Lomiri.Connectivity 1.0 //for network available check
import "components"
import "ui"

/* digest calculator functions */
import "js/hashes.js" as Hashes
import "js/base64.js" as Base64enc

/* Crypto lib for file protection */
import "js/crypto-js/crypto-js.js" as CryptoJSLib

import "js/Utility.js" as Utility


MainView {

    id: root
    objectName: "mainView"

    /* Note! applicationName needs to match the "name" field of the click manifest */
    applicationName: "tedit.danfro"

    /* application data folder where tedit files are saved (path is fixed due to app armor confinement rules) */
    property string fileSavingPath: PF.StandardPaths.writableLocation(PF.StandardPaths.AppDataLocation).toString().replace("file://","") + "/"

    automaticOrientation: true
    anchorToKeyboard: true

    theme.name: "" // System theme as default

    /*------- Tablet (width >= 110) -------- */
    //vertical
    //width: units.gu(75)
    //height: units.gu(111)

    //horizontal (for release)
    width: units.gu(100)
    height: units.gu(75)

    /* ----- phone 4.5 (the smallest one) ---- */
    //vertical
    //width: units.gu(50)
    //height: units.gu(72)

    //horizontal
    //width: units.gu(96)
    //height: units.gu(50)
    /* -------------------------------------- */

    /* the text to show in the operation result popup */
    property string infoText: ""

    /* color definitions for several purposes
       values will be set in onCompleted event */
    property color bgColorMainPage: settings.pageBackgroundColor == "theme background color" ? theme.palette.normal.background : settings.pageBackgroundColor
    property color fgColorMainPage: Qt.colorEqual(bgColorMainPage,"#ffffff") ? LomiriColors.slate : LomiriColors.silk  //when pageBG is white, set Slate, otherwise Silk

    /*
    When the app is started with clickable desktop, the check for network connection via Connectivity does not work.
    If the app is run in desktop mode, set this property to 'true' and the check will be disabled.
    */
    property bool isDesktopMode: false
    /*
      Indicates if the last API call resulted in a network error
      see https://ubports.gitlab.io/docs/api-docs/index.html?p=connectivity%2Fqml-lomiri-connectivity-networkingstatus.html
    */
    property bool networkError: isDesktopMode ? false : Connectivity.online ? false : true //derived from Connectivity, only set false for desktop mode

    Connections {
        target: Connectivity
        onOnlineChanged: {
            networkError = isDesktopMode ? false : Connectivity.online ? false : true
        }
    }

    Connections {
        target: ContentHub

        onImportRequested: {
            /* 
                if we receive a file from content hub...
                - read that file's content and decrypt if needed
                - save it as new file to tedit work folder
            */
            var contentHubPath = String(transfer.items[0].url).replace('file://', '')
            var importFileName = fileIO.getFullName(contentHubPath)
            var contentToSave = fileIO.read(contentHubPath).toString()
            var targetFilePath = "file://" + root.fileSavingPath + importFileName
            if (importFileName.includes("XXX")) {
                /* 
                    file is encrypted
                    setting file content to textarea happens in decryptKeyInput
                    automatic saving happens in decryptKeyInput
                */
                PopupUtils.open(decryptKeyInput,root,{filePath: contentHubPath})
            } else {
                textArea.text = contentToSave
                mainPage.title = importFileName;
                mainPage.saved = true  /* ie: file NOT modified yet */
                mainPage.openedFileName = importFileName;
                mainPage.fileEncrypted = false;
                mainPage.encryptionPassword = "";
                mainPage.currentFileLabelVisible = true
                fileIO.write(targetFilePath, contentToSave)
            }
        }
    }

    Settings {
        id: settings
        property string showAlert: "true"

        property int minPasswordLength: 4
        property string selectedTheme: "System"
        property bool highlightEncrypted: true

        property bool wordWrap: false
        property string pageBackgroundColor: '#1E2F3F'
        property string textAreaFontColor: "theme font color"
        property int pixelSize: 40 /* default */
    }

    /* Application main page */
    QQ.Page {
        id: mainPage
        anchors.fill: parent

        property Item textArea
        property bool saved: true /* a flag: true if file is NOT currently modified */
        property string openedFileName: ""  /* the currently opened file name */
        property bool currentFileLabelVisible: false /* to hide/show label with the current file name */
        property bool fileEncrypted: false /* set to "Encrypted" if the file is saved as encrypted */
        property string encryptionPassword: "" /* the encryptionkey of the currently opened file */

        header: PageHeader {

            id: header

            title: i18n.tr("tedit")

            leadingActionBar.actions: [
                Action {
                    iconName: "navigation-menu"
                    text: i18n.tr("Menu")
                    onTriggered: {
                        pageStack.push(menuPage)
                        menuPage.visible = true
                    }
                }
            ]

            trailingActionBar {
                actions: [
                    Action {
                        iconName: "close"
                        text: i18n.tr("Close app")
                        onTriggered: {
                            if (mainPage.saved) {
                                Qt.quit()
                            } else {
                                if (mainPage.openedFileName == "") { /* true if file is new, never saved  */
                                    PopupUtils.open(saveAsDialog,root, { 'quitOnSaved': true })
                                } else { /* file not new: already exist: just update content */
                                    PopupUtils.open(unsavedDialog,root, { 'quitOnSaved': true })
                                }
                            }
                        }
                    },
                    Action {
                        iconName: "help"
                        text: i18n.tr("About")
                        onTriggered: {
                            pageStack.push(aboutPage)
                            aboutPage.visible = true
                        }
                    },
                    Action {
                        iconName: "settings"
                        text: i18n.tr("Settings")
                        onTriggered: {
                            pageStack.push(settingsPage)
                            settingsPage.visible = true
                        }
                    }
                ]
            numberOfSlots: 3
            }
        }

        // page background for everything outside of the TextArea
        background: Rectangle {
            id: pageBG
            color: bgColorMainPage
        }

        /* show the name of the currently opened file name */
        Rectangle {
            id: currentFileOpenedContainer
            anchors {
                top: parent.top
                topMargin: units.gu(1)
                left: parent.left
                leftMargin: units.gu(2)
                right: parent.right
                rightMargin: units.gu(2)
            }

            color: bgColorMainPage
            width: parent.width
            height: units.gu(5.5)

            Row {
                id: fileNameRow

                spacing: units.gu(1)
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: units.gu(1)
                anchors.right: padlockIcon.left
                anchors.rightMargin: units.gu(1)

                Label {
                    id: storagePathLabel
                    anchors.bottom: currentFileOpenedLabel.bottom
                    anchors.bottomMargin: units.gu(0.2)
                    text: fileSavingPath.replace("/home/phablet","~") 
                    font.italic: true
                    textSize: "XSmall"
                    color: fgColorMainPage
                }

                Label {
                    id: currentFileOpenedLabel
                    anchors.verticalCenter: parent.verticalCenter
                    visible: mainPage.currentFileLabelVisible
                    color: mainPage.saved ? theme.palette.normal.positive : theme.palette.normal.negative
                    width: parent.width - storagePathLabel.width
                    text: mainPage.openedFileName
                    elide: Text.ElideRight
                    font.bold: true
                }
            }

            /* some tools from menu for quicker access also provided here */
            Row {
                id: menuButtonRow
                anchors {
                    top: fileNameRow.bottom
                    topMargin: units.gu(0.5)
                    left: parent.left
                    leftMargin: units.gu(1)
                    right: padlockIcon.left
                    rightMargin: units.gu(1)
                }
                spacing: units.gu(1.25)

                Button {
                    id: newNoteButton
                    height: units.gu(3)
                    width: height
                    color: bgColorMainPage
                    Icon {
                        anchors.centerIn: parent
                        width: parent.width
                        height: width
                        name: "note-new"
                        color: fgColorMainPage
                    }
                    opacity: enabled ? 1.0 : 0.6

                    onClicked: {
                        if (mainPage.saved == false) {
                                PopupUtils.open(confirmSavingNoteNewNote);
                        } else {
                                mainPage.openedFileName = ""
                                mainPage.currentFileLabelVisible = false
                                textArea.text = ""
                                mainPage.saved = true
                        }
                    }
                }

                Button {
                    id: openButton
                    height: units.gu(3)
                    width: height
                    color: bgColorMainPage
                    Icon {
                        anchors.centerIn: parent
                        width: parent.width
                        height: width
                        name: "document-open"
                        color: fgColorMainPage
                    }
                    opacity: enabled ? 1.0 : 0.6

                    onClicked: pageStack.push(Qt.resolvedUrl("ui/LocalFilePickerPage.qml"))
                }

                Button {
                    id: saveButton
                    height: units.gu(3)
                    width: height
                    enabled: mainPage.saved ? false : true
                    color: bgColorMainPage
                    Icon {
                        anchors.centerIn: parent
                        width: parent.width
                        height: width
                        name: "document-save"
                        color: fgColorMainPage
                    }
                    opacity: enabled ? 1.0 : 0.6

                    onClicked: {
                        if (mainPage.openedFileName == "") { /* true if file is new and has never been saved  */
                            PopupUtils.open(saveAsDialog)
                        } else { /* file is not new, it does already exist, then just update content */
                            /* function in Main.qml file */
                            saveExistingFile(mainPage.openedFileName,root.fileSavingPath)
                        }
                    }
                }

                Button {
                    id: undoButton
                    height: units.gu(3)
                    width: height
                    enabled: textArea.canUndo
                    color: bgColorMainPage
                    Icon {
                        anchors.centerIn: parent
                        width: parent.width
                        height: width
                        name: "edit-undo"
                        color: fgColorMainPage
                    }
                    opacity: enabled ? 1.0 : 0.6

                    onClicked: textArea.undo()
                }

                Button {
                    id: redoButton
                    height: units.gu(3)
                    width: height
                    enabled: textArea.canRedo
                    color: bgColorMainPage
                    Icon {
                        anchors.centerIn: parent
                        width: parent.width
                        height: width
                        name: "edit-redo"
                        color: fgColorMainPage
                    }
                    opacity: enabled ? 1.0 : 0.6

                    onClicked: textArea.redo()
                }

                Button {
                    id: selectAllButton
                    height: units.gu(3)
                    width: height
                    enabled: textArea.length //only enable when some text is available
                    color: bgColorMainPage
                    Icon {
                        anchors.centerIn: parent
                        width: parent.width
                        height: width
                        name: "edit"
                        color: fgColorMainPage
                    }
                    opacity: enabled ? 1.0 : 0.6

                    onClicked: textArea.selectAll()
                }

                Button {
                    id: copyClipboardButton
                    height: units.gu(3)
                    width: height
                    enabled: textArea.selectedText != "" //only enable when some text is selected
                    color: bgColorMainPage
                    Icon {
                        anchors.centerIn: parent
                        width: parent.width
                        height: width
                        name: "edit-copy"
                        color: fgColorMainPage
                    }
                    opacity: enabled ? 1.0 : 0.6

                    onClicked: {
                        var content = Clipboard.newData()
                        content = textArea.text
                        Clipboard.push(content)
                    }
                }

                Button {
                    id: pasteClipboardButton
                    height: units.gu(3)
                    width: height
                    enabled: Clipboard.data.text
                    color: bgColorMainPage
                    Icon {
                        anchors.centerIn: parent
                        width: parent.width
                        height: width
                        name: "edit-paste"
                        color: fgColorMainPage
                    }
                    opacity: enabled ? 1.0 : 0.6

                    onClicked: {
                        if (Clipboard.data.text) {
                            PopupUtils.open(confirmPasteFromClipboard)
                        } else {
                            showInfo(i18n.tr("Clipboard is empty, nothing there to paste."))
                        }
                    }
                }

                Button {
                    id: clearAllButton
                    height: units.gu(3)
                    width: height
                    enabled: textArea.length //only enable when some text is available
                    color: bgColorMainPage
                    Icon {
                        anchors.centerIn: parent
                        width: parent.width
                        height: width
                        name: "delete"
                        color: fgColorMainPage
                    }
                    opacity: enabled ? 1.0 : 0.6

                    onClicked: PopupUtils.open(confirmClearAll)
                }

                Button {
                    id: clearClipboardButton
                    height: units.gu(3)
                    width: height
                    color: bgColorMainPage
                    Icon {
                        anchors.centerIn: parent
                        width: parent.width
                        height: width
                        name: "edit-clear"
                        color: fgColorMainPage
                    }
                    opacity: enabled ? 1.0 : 0.6

                    onClicked: {
                        // "Clipboard.clear" seems not to work
                        // just pushing an empty string breaks copying to the clipboard
                        var empty = Clipboard.newData()
                        Clipboard.push(empty)
                        showInfo(i18n.tr("Clipboard cleared"))
                    }
                }
            }

            Image {
                id: padlockIcon
                visible: mainPage.fileEncrypted
                source: Qt.resolvedUrl("../assets/encrypted.png")
                height: units.gu(2)
                width: visible ? height : 0

                anchors {
                    right: parent.right
                    rightMargin: units.gu(1)
                    top: parent.top
                }
            }
        }

        Flickable {
            id: resultPageFlickable
            clip: true
            flickableDirection: Flickable.AutoFlickIfNeeded
            boundsBehavior: Flickable.StopAtBounds
            height: mainPage.height - (header.height + currentFileOpenedContainer.height + counterOnOffContainer.height + Qt.inputMethod.keyboardRectangle.height)
            anchors {
                top: currentFileOpenedContainer.bottom
                topMargin: units.gu(1)
                left: parent.left
                right: parent.right
                bottom: counterOnOffContainer.top
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
                bottomMargin: units.gu(0.5)
            }

            Rectangle {
                id: textAreaContainer
                width: parent.width
                height: parent.height
                radius: 20
                border.color : theme.palette.normal.backgroundTertiaryText
                border.width : units.gu(0.1)

                /* Display the file content */
                TextArea {
                    id: textArea

                    anchors.centerIn: parent
                    width: parent.width - (2 * parent.border.width)
                    height: parent.height - (2 * parent.border.width)
                    textFormat: TextEdit.AutoText
                    font.pixelSize: settings.pixelSize
                    color: settings.textAreaFontColor == "theme font color" ? theme.palette.normal.foregroundText : settings.textAreaFontColor
                    placeholderText: i18n.tr("Welcome ! Write what you want and save it. Enjoy!")
                    onTextChanged: {
                        /* update flag file modified and not saved */
                        mainPage.saved = false;
                    }
                    wrapMode: settings.wordWrap ? TextEdit.Wrap : TextEdit.NoWrap

                // onLinkActivated: Qt.resolvedUrl(link)
                }
            }
        } //flick

        Rectangle {
            id: counterOnOffContainer
            color: "transparent"
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                bottomMargin: units.gu(1)
            }
            width: parent.with
            height: units.gu(3)
            Label {
                id: counterLabel
                anchors {
                    left: parent.left
                    leftMargin: units.gu(3)
                    verticalCenter: parent.verticalCenter
            }
            height: units.gu(2)
                text: i18n.tr("Line count") + ": " + textArea.lineCount + "  " + i18n.tr("Character count") + ": " + textArea.length
                font.italic: true
                color: fgColorMainPage
            }
        }
    }

    Component.onCompleted: {
        if (settings.showAlert == "true") {
            showInfo("<b>" + i18n.tr("If the encryption key is lost, there is NO restore. The file remains encrypted! The app author takes no responsibility!") + "</b>" + "\n" + i18n.tr("Encrypted file names have a \"XXX\" suffix. A padlock is shown, when an encrypted file is currently open."));
            settings.showAlert = "false"
        }
        setCurrentTheme()
    }

    onThemeChanged: setCurrentTheme()

    /* To notify messages at the user */
    Component {
        id: popover
        Dialog {
            id: po
            text: infoText
            Button {
                text: i18n.tr("OK")
                width: units.gu(7)
                onClicked: PopupUtils.close(po)
            }
        }
    }

    /* Menu Chooser */
    Component {
        id: menuPopover
        Dialog {
            id: menuDialog
            text: infoText
            MouseArea {
                anchors.fill: parent
                onClicked: PopupUtils.close(menuDialog)
            }
        }
    }

    Component {
        id: confirmSavingNoteNewNote
        ConfirmSavingNoteNewNote { }
    }

    Component {
        id: confirmClearAll
        ConfirmClearAll{}
    }

    Component {
        id: confirmPasteFromClipboard
        ConfirmPasteFromClipboard{}
    }

    /* custom C++ plugin to save/manage files on the filesystem */
    FileIO {
        id: fileIO
    }

    SettingsPage {
        id: settingsPage
    }

    AboutPage {
        id: aboutPage
    }

    UnsavedDialog  {
        id: unsavedDialog;
        property Action closeAction
    }

    /* PopUp with the menu */
    MenuOptions {
        id: menuPage
    }

    /* Ask for a web-site url to import as text in the textArea */
    Component {
        id: webSiteSelector
        WebSiteSelector{}
    }

    /* Ask for a web-site url to generate the associated QR Code */
    Component {
        id: qrCodeWebSiteSelector
        QrCodeWebSiteSelector{}
    }

    Component {
        id: saveAsDialog
        SaveAsDialog{}
    }

    Base64conversionPage {
        id: base64conversionPage
    }

    QrCodeGeneratorPage {
        id: qrCodeGeneratorPage
    }

    /* renderer for the entry in the Digest OptionSelector */
    Component {
        id: digestChooserDelegate
        OptionSelectorDelegate { text: name; }
    }

    Component {
        id: digestCalculatorChooser
        DigestCalculator{inputText: textArea.text}
    }

    Component {
        id: decryptKeyInput
        DecryptKeyInput {}
    }

    PageStack {
        id: pageStack

        Component.onCompleted: {
            pageStack.push( mainPage )
        }
    } //pageStack

    Timer {
        // the r/w process when saving a file does take a small time
        // so we allow a small delay after pressing the button before the next action
        id: quitTimer
        interval: 350
        running: false
        repeat: false
        onTriggered: Qt.quit();
    }

    /*
        Encrypt the provided content using the provided key
    */
    function encrypt(content, key) {
        var iv= '16BytesLengthKey';
        var CryptoJS = CryptoJSLib.CryptoJS;
        var AES = CryptoJS.AES;
        var ivStr  = CryptoJS.enc.Utf8.parse(iv);
        var keyStr = CryptoJS.enc.Utf8.parse(key);

        var text = AES.encrypt(content, keyStr, {
                                    iv: ivStr,
                                    mode: CryptoJS.mode.OFB,
                                    padding: CryptoJS.pad.Iso10126
                                });
        return text.toString();
    }

    /*
        DEcrypt the provided ciphertext using the provided key
    */
    function decrypt(ciphertext, key) {
            var iv= '16BytesLengthKey';
            var CryptoJS = CryptoJSLib.CryptoJS;
            var AES = CryptoJS.AES;
            var ivStr  = CryptoJS.enc.Utf8.parse(iv);
            var keyStr = CryptoJS.enc.Utf8.parse(key);

            /* Note: decrypt works only with this values of 'mode' and 'padding' */
            var bytes = AES.decrypt(ciphertext, keyStr, {
                                        iv: ivStr,
                                        mode: CryptoJS.mode.OFB,
                                        padding: CryptoJS.pad.Iso10126
                                    });

            var plaintext = bytes.toString(CryptoJS.enc.Utf8);

            return plaintext.toString();
    }

    /*
        Used when the user press "Save" button in the menu for an already saved file
        whose content is just updated
    */
        function saveExistingFile(filename, destinationDir) {

            /* console.log("Saving existing file..."); */

            /* path and fileName */
            var destinationFolder = "file://" + destinationDir + fileIO.getFullName(filename);

            /* dummy solution to prevent content append */
            if (fileIO.exists(destinationFolder)){
                fileIO.remove(destinationFolder)
            }

            var contentToSave;
            /* current file could be previously saved as Encrypted or not... */
            if (mainPage.fileEncrypted === true){
                contentToSave = encrypt(textArea.text, mainPage.encryptionPassword);
            } else {
                contentToSave = textArea.text;
            }

            fileIO.write(destinationFolder, contentToSave);

            /* to prevent opening UnSavedDialog */
            mainPage.saved = true;
        }

    /* show a Popup containing the provided argument as text */
    function showInfo(info) {
        infoText = "\n" + info + "\n";
        PopupUtils.open(popover);
    }

    function setCurrentTheme() {
        if (settings.selectedTheme == "System") {
          theme.name = "";
        } else if (settings.selectedTheme == "SuruDark") {
          theme.name = "Lomiri.Components.Themes.SuruDark"
        } else if (settings.selectedTheme == "Ambiance") {
          theme.name = "Lomiri.Components.Themes.Ambiance"
        } else {
          theme.name = "";
        }
    }
}
